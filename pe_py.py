#!/usr/bin/python
import pefile
import distorm3
import os
from pprint import pprint

file1="ddd.old"
file2="eee.old"

#### functions ####
def get_sect_of_entrypoint(filename):
    pe = pefile.PE(filename)
    addr_of_entry_point = pe.OPTIONAL_HEADER.AddressOfEntryPoint
    for a_section in pe.sections:
        if a_section.PointerToRawData < addr_of_entry_point and\
        addr_of_entry_point < a_section.PointerToRawData+a_section.SizeOfRawData:
            break
    return a_section


# print pe.dump_info()
def getMachine(filename):
    pe = pefile.PE(filename)
    machine = pe.FILE_HEADER.Machine

    # # 1) Get machine type
    if pe.FILE_HEADER.Machine==0x14C:
        machine_type="x86"
    elif pe.FILE_HEADER.Machine==0x0200:
        machine_type="Intel Itanium"
    else:
        machine_type="x64"
    return machine_type

print "*************************************"
print "[#1.1: Machine type:]", getMachine(file1)
print "[#1.2: Machine type:]", getMachine(file2)
print "*************************************\n"

#  # 2) Get number of sections
pe1 = pefile.PE(file1)
pe2 = pefile.PE(file2)
nsect1 = pe1.FILE_HEADER.NumberOfSections
nsect2 = pe2.FILE_HEADER.NumberOfSections
print "*************************************"
print "[#2.1: Number of sections:]", nsect1
print "[#2.1: Number of sections:]", nsect2
print "*************************************\n"

# # 3) Number of executable sections
def executable_sections(filename):
    pe = pefile.PE(filename)
    print "*************************************"
    print "[#3: Number of eXectutable sections:]"
    nb_executable_sections = 0
    for idx, a_section in enumerate(pe.sections):
        if a_section.__dict__.get('IMAGE_SCN_MEM_EXECUTE')==False:
            print "Section#"+str(idx)+"[name="+a_section.Name+"]","[",a_section.PointerToRawData, ",", \
                a_section.PointerToRawData+a_section.SizeOfRawData,"] NOT executable"
        else:
            print "Section#" + str(idx) + "[name=" + a_section.Name + "]", "[", a_section.PointerToRawData, ",", \
            a_section.PointerToRawData + a_section.SizeOfRawData, "] IS executable"
            nb_executable_sections+=1
    print nb_executable_sections, " section(s) is(are) executable"
    print "*************************************\n"

executable_sections(file1)
executable_sections(file2)

# # 4) Section cointaining EntryPoint
print "*************************************"
print "[#4: Section containing EntryPoint:]"
section_of_entry_point = get_sect_of_entrypoint(file1)
print "This section contains EntryPoint ->", section_of_entry_point.Name
print "*************************************\n"

# # 5) The position of the EntryPoint in the section it resides in
def wheresmyentrypoint(filename):
    pe = pefile.PE(filename)
    print "*************************************"
    address_of_entry_point =  pe.OPTIONAL_HEADER.AddressOfEntryPoint
    address_of_entry_point_container = section_of_entry_point.PointerToRawData
    print "[#5: EntryPoint position in its section:] ", format( ((address_of_entry_point-address_of_entry_point_container)/ \
                             (float)(section_of_entry_point.SizeOfRawData)), '.2f')
    print "*************************************\n"
# 6) Call disassembler on each executable section
# 7) Also, create 5-grames
def get_n_grame(filename):
    pe = pefile.PE(filename)
    executable_sections_vct=[]
    executable_files_vct=[]
    code = open(filename, 'rb')
    # gather hex code of executable sections in some files
    for idx, a_section in enumerate(pe.sections):
        if a_section.__dict__.get('IMAGE_SCN_MEM_EXECUTE')==True:
            executable_sections_vct.append(a_section)
            a_file = open(filename+str(idx)+".txt", "wb")
            a_file.seek(0)
            a_file.truncate()
            code.seek(a_section.PointerToRawData)
            section_contents = code.read(a_section.SizeOfRawData)
            a_file.write(section_contents)
            executable_files_vct.append(a_file)
            a_file.close()

        # code = open("1st_encounter.dontrun", 'rb').read()

    a_dict = dict()
    for idx, a_section in enumerate(executable_sections_vct):
        a_file = open(executable_files_vct[idx].name, 'rb').read()
        iterable = distorm3.DecodeGenerator(a_section.PointerToRawData, a_file, distorm3.Decode32Bits)
        # Iterate over the extracted instructions from the section
        list_of_filtered = []
        total = 0
        for idx, (offset, size, instruction, hexdump) in enumerate(iterable):
            op_code = instruction.split(' ', 1)[0]
            if not "NOP" in op_code and not "INT" in op_code:
                if "ADD" in op_code or "SUB" in op_code or "INC" in op_code:
                    op_code = "***"
                total += 1
                list_of_filtered.append(op_code)
        print "[debug]TOTAL", total
    # pprint(list_of_filtered)

    step = 5

    five_gram_set = set()
    a_five_gram = ""

    # N-Grame logic below
    for i in range(0, total-step, 1):
        for j in range(i, i+step, 1):
            a_five_gram+=list_of_filtered[j]
        # print a_five_gram
        five_gram_set.add(a_five_gram)
        a_five_gram = ""

    # for a_file in executable_files_vct:
    #     os.remove(a_file.name)

    return five_gram_set
    # pprint(five_gram_set)


# # 8
set1 = get_n_grame(file1)
set2 = get_n_grame(file2)

print "Dimension of set1:", len(set1)
print "Dimension of set2:", len(set2)
intersection = set.intersection(set1, set2)
print "Intersection: Number of common elems:", len(intersection)
union = set.union(set1, set2)
print "Union: Number of elems", len(union)
jaccard = (float)(len(intersection)) / len(union)
print "Result is:" "Intersection / Union", jaccard
print "READY"







